package model.user;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.sound.sampled.*;

class JavaAudio {
    public static void main(String[] args) throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        Scanner scanner = new Scanner(System.in);

        File musicDirectory = new File("C:\\Users\\SOC\\Desktop\\MusicPlaylist\\songs");
        File[] musicFiles = musicDirectory.listFiles((dir, name) -> name.toLowerCase().endsWith(".wav"));

        if (musicFiles == null || musicFiles.length == 0) {
            System.out.println("No music files found in the directory.");
            return;
        }

        Clip[] clips = new Clip[musicFiles.length];
        File[] playlist = new File[musicFiles.length];
        List<File> customPlaylist = new ArrayList<>(); // User-defined playlist

        for (int i = 0; i < musicFiles.length; i++) {
            System.out.println((i + 1) + ": " + musicFiles[i].getName());
        }

        System.out.print("Enter the number of the song you want to select (or 'Q' to quit): ");
        String response = scanner.next();

        if (response.equalsIgnoreCase("Q")) {
            System.out.println("Bye!");
            return;
        }

        int selectedSongIndex;
        try {
            selectedSongIndex = Integer.parseInt(response);
        } catch (NumberFormatException e) {
            System.out.println("Invalid input. Exiting.");
            return;
        }

        if (selectedSongIndex < 1 || selectedSongIndex > musicFiles.length) {
            System.out.println("Invalid song number. Exiting.");
            return;
        }

        File selectedSong = musicFiles[selectedSongIndex - 1];
        AudioInputStream audioStream = AudioSystem.getAudioInputStream(selectedSong);
        Clip clip = AudioSystem.getClip();
        clip.open(audioStream);
        clips[0] = clip;

        System.out.println(
                "P = play, S = Stop, R = Reset, N = Next song, A = Add to playlist, L = Play playlist, C = Create playlist, Q = Quit");

        while (true) {
            System.out.print("Enter your choice: ");
            response = scanner.next();
            response = response.toUpperCase();

            switch (response) {
                case "P":
                    clip.start();
                    break;
                case "S":
                    clip.stop();
                    break;
                case "R":
                    clip.setMicrosecondPosition(0);
                    break;
                case "N":
                    int nextSongIndex = (selectedSongIndex + 1) % musicFiles.length;
                    selectedSong = musicFiles[nextSongIndex];
                    audioStream = AudioSystem.getAudioInputStream(selectedSong);

                    if (clips[0] != null) {
                        clips[0].close();
                    }

                    clip = AudioSystem.getClip();
                    clip.open(audioStream);
                    clips[0] = clip;
                    selectedSongIndex = nextSongIndex;
                    clip.start();
                    break;
                case "A":
                    playlist[selectedSongIndex - 1] = selectedSong;
                    System.out.println("Added to playlist.");
                    break;
                case "C": // Create a new playlist
                    customPlaylist.clear(); // Clear the existing custom playlist
                    System.out.print("Enter a name for the new playlist: ");
                    String playlistName = scanner.next();
                    System.out.println("Playlist '" + playlistName + "' created.");
                    break;
                case "ADD": // Add the selected song to the custom playlist
                    if (customPlaylist.isEmpty()) {
                        System.out.println("Create a playlist first (C).");
                    } else {
                        customPlaylist.add(selectedSong);
                        System.out.println("Added to the custom playlist.");
                    }
                    break;
                case "L":
                    if (playlist[0] != null) {
                        for (File song : playlist) {
                            if (song != null) {
                                audioStream = AudioSystem.getAudioInputStream(song);

                                if (clips[0] != null) {
                                    clips[0].close();
                                }

                                clip = AudioSystem.getClip();
                                clip.open(audioStream);
                                clips[0] = clip;
                                clip.start();
                                clip.drain();
                            }
                        }
                    } else {
                        System.out.println("Playlist is empty.");
                    }
                    break;
                case "Q":
                    if (clips[0] != null) {
                        clips[0].close();
                    }
                    break;
                default:
                    System.out.println("Not a valid response");
            }

            if (response.equals("Q")) {
                System.out.println("Bye!");
                break;
            }
        }

    }
}



// package model.user;
// import java.util.Scanner;

// class Music {

// }
// class Main {
//     public static void main(String[] args) {
//         System.out.println("Welcome to music playlist sorter!");

//         // Create a scanner for user input
//         Scanner scan = new Scanner(System.in);
//         Users users = new Users(10);
//         User loggedInUser = null;
//         boolean loggedIn = false;
//         while (!loggedIn) {
//             // Ask the user for their choice
//             System.out.println("Enter 1: For registration");
//             System.out.println("Enter 2: For login");
//             int option = scan.nextInt();
//             scan.nextLine();
//             if (option == 1) {
//                 // User Registration
//                 System.out.println("Registration:");
//                 System.out.println("Enter your name: ");
//                 String name = scan.nextLine();
//                 System.out.println("Enter your email: ");
//                 String email = scan.nextLine();
//                 System.out.println("Enter your password: ");
//                 String password = scan.nextLine();

//                 User user = new User(name, email, password);
//                 users.register(user);
//                 System.out.println(users.count);

//                 System.out.println("Registration successful!");
//             } else if (option == 2) {
//                 // User Login
//                 System.out.println("Login:");
//                 System.out.print("Enter your email: ");
//                 String email = scan.nextLine();
//                 System.out.print("Enter your password: ");
//                 String password = scan.nextLine();

//                 loggedInUser = users.login(email, password);

//                 if (loggedInUser != null) {
//                     System.out.println("Login successful!");
//                     System.out.println("Add the musics functions! ");
//                     // Add your music management code here
//                 } else {
//                     System.out.println("Login failed. Invalid credentials.");
//                 }
//             }

//         }

//         // Music management code can be placed here, and the user is logged in.

//         scan.close();
//     }
// }



// // AVL Tree is better for search 