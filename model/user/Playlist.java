package model.user;

import javax.sound.sampled.*;
import java.io.*;
import java.util.Arrays;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Playlist {
    private String userPlaylistFolderPath;
    private String[] playlist;
    private int playlistSize;
    private Clip currentClip = null;
    private int currentSongIndex = 0; // Added currentSongIndex variable
    private boolean userStopped = false;

    public Playlist(String playlistFilePath, String userPlaylistFolderPath, String username) {
        this.userPlaylistFolderPath = userPlaylistFolderPath + File.separator + username;
        this.playlist = readPlaylist(playlistFilePath);
        this.playlist = (this.playlist == null) ? new String[10] : this.playlist;

        // Ensure the user's playlist folder exists
        File userPlaylistFolder = new File(userPlaylistFolderPath, username);
        if (!userPlaylistFolder.exists()) {
            userPlaylistFolder.mkdirs();
        }

        // Create a new playlist file if it doesn't exist
        File playlistFile = new File(userPlaylistFolder, "playlist.txt");
        if (!playlistFile.exists()) {
            createNewPlaylistFile(playlistFile);
        }
    }

    public String[] getPlaylist() {
        return playlist;
    }

    public void addSongToPlaylist(Song song, String playlistFolder, String userName) {
        String songInfo = song.getTitle() + " - " + song.getArtist() + " - " + song.getFilePath();

        // Ensure the user's playlist folder exists
        File userPlaylistFolder = new File(userPlaylistFolderPath, userName);
        if (!userPlaylistFolder.exists()) {
            userPlaylistFolder.mkdirs();
        }

        // Create a new playlist file if it doesn't exist
        File playlistFile = new File(userPlaylistFolder, "playlist.txt");
        if (!playlistFile.exists()) {
            createNewPlaylistFile(playlistFile);
        }

        // Debug prints
        System.out.println("Debug: playlistSize = " + playlistSize);
        System.out.println("Debug: playlist length = " + (playlist != null ? playlist.length : "null"));

        // Check if the song is already in the playlist
        if (isSongInPlaylist(songInfo)) {
            System.out.println("Error: Song is already in the playlist.");
            return;
        }

        // If the playlist is empty, initialize it
        if (playlist == null) {
            playlist = new String[10]; // Adjust the initial size based on your needs
            System.out.println("Debug: playlist initialized");
        }

        // If the playlist is full, resize it
        if (playlistSize == playlist.length) {
            playlist = resizeArray(playlist);
            System.out.println("Debug: playlist resized");
        }

        // Add the song to the playlist
        playlist[playlistSize] = songInfo;
        playlistSize++;

        // Write the song to the playlist file
        try (FileWriter writer = new FileWriter(playlistFile, true);
                BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            bufferedWriter.write(songInfo);
            bufferedWriter.newLine();

            System.out.println("Song added to the playlist successfully!");

        } catch (IOException e) {
            System.err.println("Error: Unable to add the song to the playlist. " + e.getMessage());
        }
    }

    // Check if the song is already in the playlist
    private boolean isSongInPlaylist(String songInfo) {
        if (playlist != null) {
            for (int i = 0; i < playlistSize; i++) {
                if (playlist[i].equals(songInfo)) {
                    return true;
                }
            }
        }
        return false;
    }

    private String extractFilePath(String songInfo) {
        // Assuming the file path is the concatenation of all parts after the first two
        String[] parts = songInfo.split(" - ");

        if (parts.length >= 2) {
            String songName = parts[0].trim(); // Use the first part as the song name
            String artistName = parts[1].trim(); // Use the second part as the artist name

            // Assuming your sourceFolder is the directory where your songs are stored
            File sourceFolder = new File("C://Users//SOC//Desktop//project//model//listofsong");

            if (sourceFolder.isDirectory()) {
                // Combine the remaining parts to get the file path
                String filePath = String.join(" - ", Arrays.copyOfRange(parts, 2, parts.length)).trim();

                File[] matchingFiles = sourceFolder
                        .listFiles((dir, name) -> name.toLowerCase().startsWith(songName.toLowerCase()) &&
                                name.toLowerCase().contains(artistName.toLowerCase()) &&
                                name.endsWith(".mp3"));

                if (matchingFiles != null && matchingFiles.length > 0) {
                    return matchingFiles[0].getAbsolutePath();
                } else {
                    System.out.println("Error: File not found for song - " + songInfo);
                }
            } else {
                System.out.println("Error: Invalid source folder path. Please check the configuration.");
            }
        } else if (parts.length == 1) {
            // Handle single-part songInfo (assuming the single part is the file name)
            String fileName = parts[0].trim();
            // Assuming your sourceFolder is the directory where your songs are stored
            File sourceFolder = new File("C://Users//SOC//Desktop//project//model//listofsong");

            if (sourceFolder.isDirectory()) {
                File[] matchingFiles = sourceFolder
                        .listFiles((dir, name) -> name.toLowerCase().startsWith(fileName.toLowerCase()) &&
                                name.endsWith(".mp3"));

                if (matchingFiles != null && matchingFiles.length > 0) {
                    return matchingFiles[0].getAbsolutePath();
                } else {
                    System.out.println("Error: File not found for song - " + songInfo);
                }
            } else {
                System.out.println("Error: Invalid source folder path. Please check the configuration.");
            }
        } else {
            System.out.println("Error: Invalid songInfo format. Expected at least 1 part.");
        }

        return ""; // You might want to handle this more gracefully
    }

    public void playAudio() {
        if (playlistSize == 0) {
            System.out.println("Playlist is empty. Add songs to play.");
        } else {
            String songInfo = playlist[0];
            String songFilePath = extractFilePath(songInfo);

            if (!songFilePath.isEmpty()) {
                System.out.println("Debug: Song File Path: " + songFilePath); // Debug print
                System.out.println("Playing: " + songInfo);

                try {
                    File file = new File(songFilePath);
                    if (file.exists()) {
                        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(file);
                        currentClip = AudioSystem.getClip();

                        // Rest of the code remains unchanged
                        currentClip.open(audioInputStream);

                        // Add a LineListener to detect when playback is complete
                        currentClip.addLineListener(event -> {
                            if (event.getType() == LineEvent.Type.STOP) {
                                System.out.println("Finished playing: " + songInfo);
                                currentClip.close();
                            }
                        });

                        currentClip.start();
                    } else {
                        System.out.println("Error: File does not exist - " + songFilePath);
                    }
                } catch (UnsupportedAudioFileException | LineUnavailableException | IOException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("Error: Invalid songInfo format. Expected at least 3 parts.");
                System.out.println("Actual songInfo format: " + songInfo);
            }
        }
    }

    private String[] readPlaylist(String playlistFilePath) {
        File playlistFile = new File(playlistFilePath);

        // Check if the file exists
        if (!playlistFile.exists()) {
            System.err.println("Error: Playlist file not found at " + playlistFilePath);
            return new String[0]; // Return an empty array or handle it according to your application's logic
        }

        String[] playlistArray = new String[10]; // Initial size, adjust based on your needs
        int playlistSize = 0;

        try (BufferedReader reader = new BufferedReader(new FileReader(playlistFile))) {
            String line;
            while ((line = reader.readLine()) != null) {
                if (playlistSize == playlistArray.length) {
                    // Resize the array if it's full
                    playlistArray = resizeArray(playlistArray);
                }
                playlistArray[playlistSize] = line;
                playlistSize++;
            }
        } catch (IOException e) {
            System.err.println("Error: Unable to read the playlist. " + e.getMessage());
        }

        // Trim the array to the actual size
        return arrayCopyOf(playlistArray, playlistSize);
    }

    private String[] resizeArray(String[] array) {
        int newSize = (array.length == 0) ? 1 : array.length * 2; // Adjust the resizing strategy based on your needs
        String[] newArray = new String[newSize];
        arrayCopy(array, 0, newArray, 0, array.length);
        return newArray;
    }

    private void createNewPlaylistFile(File playlistFile) {
        try {
            playlistFile.createNewFile();
            System.out.println("New playlist file created: " + playlistFile.getAbsolutePath());
        } catch (IOException e) {
            System.err.println("Error: Unable to create a new playlist file. " + e.getMessage());
        }
    }

    private void arrayCopy(Object src, int srcPos, Object dest, int destPos, int length) {
        if (src instanceof String[] && dest instanceof String[]) {
            String[] srcArray = (String[]) src;
            String[] destArray = (String[]) dest;
            for (int i = 0; i < length; i++) {
                destArray[destPos + i] = srcArray[srcPos + i];
            }
        }
    }

    private String[] arrayCopyOf(String[] original, int newLength) {
        String[] copy = new String[newLength];
        arrayCopy(original, 0, copy, 0, Math.min(original.length, newLength));
        return copy;
    }

    public void stopAudio() {
        if (currentClip != null && currentClip.isOpen()) {
            currentClip.stop();
            currentClip.close();
            System.out.println("Song stopped.");
            userStopped = true; // Set the flag to true when the user stops the audio
        } else {
            System.out.println("No song is currently playing.");
        }
    }

    public void playNext() {
        if (playlistSize == 0) {
            System.out.println("Playlist is empty. Add songs to play.");
        } else {
            // Stop the current song if it is playing
            stopAudio();

            // If the user explicitly stopped the audio, do not proceed to the next song
            if (userStopped) {
                userStopped = false; // Reset the flag
                return;
            }

            // Increment the index to play the next song
            currentSongIndex = (currentSongIndex + 1) % playlistSize;

            String nextSongInfo = playlist[currentSongIndex];
            String nextSongFilePath = extractFilePath(nextSongInfo);

            if (!nextSongFilePath.isEmpty()) {
                System.out.println("Debug: Next Song File Path: " + nextSongFilePath);
                System.out.println("Playing Next: " + nextSongInfo);

                try {
                    File file = new File(nextSongFilePath);
                    if (file.exists()) {
                        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(file);
                        currentClip = AudioSystem.getClip();

                        currentClip.open(audioInputStream);

                        // Add a LineListener to detect when playback is complete
                        currentClip.addLineListener(event -> {
                            if (event.getType() == LineEvent.Type.STOP) {
                                System.out.println("Finished playing: " + nextSongInfo);
                                currentClip.close();
                                // Automatically play the next song in the playlist
                                playNext();
                            }
                        });

                        currentClip.start();
                    } else {
                        System.out.println("Error: File does not exist - " + nextSongFilePath);
                    }
                } catch (UnsupportedAudioFileException | LineUnavailableException | IOException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("Error: Invalid songInfo format for the next song. Expected at least 3 parts.");
                System.out.println("Actual songInfo format: " + nextSongInfo);
            }
        }
    }

    public void searchAndPlay(String keyword) {
        // Search for the song in the playlist based on the keyword
        for (int i = 0; i < playlistSize; i++) {
            if (playlist[i].toLowerCase().contains(keyword.toLowerCase())) {
                // Found a match, stop the current song (if any) and play the selected song
                stopAudio();

                String selectedSongInfo = playlist[i];
                String selectedSongFilePath = extractFilePath(selectedSongInfo);

                if (!selectedSongFilePath.isEmpty()) {
                    System.out.println("Debug: Selected Song File Path: " + selectedSongFilePath);
                    System.out.println("Playing Selected Song: " + selectedSongInfo);

                    try {
                        File file = new File(selectedSongFilePath);
                        if (file.exists()) {
                            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(file);
                            currentClip = AudioSystem.getClip();

                            currentClip.open(audioInputStream);

                            // Add a LineListener to detect when playback is complete
                            currentClip.addLineListener(event -> {
                                if (event.getType() == LineEvent.Type.STOP) {
                                    System.out.println("Finished playing: " + selectedSongInfo);
                                    currentClip.close();
                                }
                            });

                            currentClip.start();
                        } else {
                            System.out.println("Error: File does not exist - " + selectedSongFilePath);
                        }
                    } catch (UnsupportedAudioFileException | LineUnavailableException | IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    System.out.println(
                            "Error: Invalid songInfo format for the selected song. Expected at least 3 parts.");
                    System.out.println("Actual songInfo format: " + selectedSongInfo);
                }

                // Exit the loop after playing the selected song
                return;
            }
        }

        // If no match is found, print an error message
        System.out.println("Error: Song not found in the playlist - " + keyword);
    }
}

class PlaylistUI extends JFrame {
    private Playlist playlist;
    private User loggedInUser;

    private JTextField songTitleField;
    private JTextField artistNameField;
    private JTextField filePathField;
    private JTextField searchKeywordField;
    private JTextArea playlistTextArea;

    public PlaylistUI(Playlist playlist, User loggedInUser) {
        this.loggedInUser = loggedInUser;
        this.playlist = playlist;

        setTitle("Playlist Player");
        setSize(600, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        initComponents();
        initPlaylistTextArea();
    }
    // ... existing methods

    // Add the logout() method here
    public void logout() {
        // Create UserLoginUI and make it visible
        UserLoginUI userLoginUI = new UserLoginUI();
        userLoginUI.setVisible(true);

        // Close the playlist window
        dispose();
    }

    // ... existing methods

    private void initComponents() {
        JPanel controlPanel = new JPanel();
        controlPanel.setLayout(new GridLayout(3, 1));

        // Add Song Panel
        JPanel addSongPanel = new JPanel();
        addSongPanel.setBorder(BorderFactory.createTitledBorder("Add Song"));
        addSongPanel.setLayout(new GridLayout(3, 2));

        songTitleField = new JTextField();
        artistNameField = new JTextField();
        filePathField = new JTextField();

        addSongPanel.add(new JLabel("Song Title:"));
        addSongPanel.add(songTitleField);
        addSongPanel.add(new JLabel("Artist Name:"));
        addSongPanel.add(artistNameField);
        addSongPanel.add(new JLabel("File Path:"));
        addSongPanel.add(filePathField);

        JButton addSongButton = new JButton("Add Song");
        addSongButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String title = songTitleField.getText();
                String artist = artistNameField.getText();
                String filePath = filePathField.getText();

                if (!title.isEmpty() && !artist.isEmpty() && !filePath.isEmpty()) {
                    Song song = new Song(title, artist, filePath);
                    playlist.addSongToPlaylist(song, "path/to/playlist", "username");
                    updatePlaylistTextArea();
                } else {
                    JOptionPane.showMessageDialog(null, "Please fill in all fields.");
                }
            }
        });

        controlPanel.add(addSongPanel);
        controlPanel.add(addSongButton);

        // Playback Controls Panel
        JPanel playbackControlsPanel = new JPanel();
        playbackControlsPanel.setBorder(BorderFactory.createTitledBorder("Playback Controls"));
        playbackControlsPanel.setLayout(new FlowLayout());

        JButton playButton = new JButton("Play");
        playButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                playlist.playAudio();
                updatePlaylistTextArea();
            }
        });

        JButton stopButton = new JButton("Stop");
        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                playlist.stopAudio();
                updatePlaylistTextArea();
            }
        });

        JButton nextButton = new JButton("Next");
        nextButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                playlist.playNext();
                updatePlaylistTextArea();
            }
        });

        playbackControlsPanel.add(playButton);
        playbackControlsPanel.add(stopButton);
        playbackControlsPanel.add(nextButton);

        JButton listSongsButton = new JButton("List Songs");
        listSongsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                listSongs();
            }
        });
        controlPanel.add(listSongsButton);

        // Search Panel
        JPanel searchPanel = new JPanel();
        searchPanel.setBorder(BorderFactory.createTitledBorder("Search Song"));
        searchPanel.setLayout(new FlowLayout());

        searchKeywordField = new JTextField();
        JButton searchButton = new JButton("Search");
        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String keyword = searchKeywordField.getText();
                if (!keyword.isEmpty()) {
                    playlist.searchAndPlay(keyword);
                    updatePlaylistTextArea();
                } else {
                    JOptionPane.showMessageDialog(null, "Please enter a keyword.");
                }
            }
        });

        searchPanel.add(new JLabel("Keyword:"));
        searchPanel.add(searchKeywordField);
        searchPanel.add(searchButton);

        controlPanel.add(playbackControlsPanel);
        controlPanel.add(searchPanel);

        add(controlPanel, BorderLayout.NORTH);

        JButton logoutButton = new JButton("Logout");
        logoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                logout();
            }
        });
        controlPanel.add(logoutButton);
    }

    private void initPlaylistTextArea() {
        playlistTextArea = new JTextArea();
        playlistTextArea.setEditable(false);

        JScrollPane scrollPane = new JScrollPane(playlistTextArea);
        add(scrollPane, BorderLayout.CENTER);

        updatePlaylistTextArea();
    }

    private void updatePlaylistTextArea() {
        playlistTextArea.setText("");
        for (String songInfo : playlist.getPlaylist()) {
            playlistTextArea.append(songInfo + "\n");
        }
    }

    private void listSongs() {
        // Clear the existing playlist text area
        playlistTextArea.setText("");

        // Get the songs with names using the getSongListWithNames method from
        // SongManager
        String[] songsWithName = SongManager.getSongListWithNames();

        if (songsWithName != null) {
            // Print the list of songs
            for (String songInfo : songsWithName) {
                playlistTextArea.append(songInfo + "\n");
            }
        } else {
            System.out.println("Error: Unable to retrieve the list of songs.");
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                User user = new User("name", "email", "password");
                Playlist playlist = new Playlist(
                        "C://Users//SOC//Desktop//project//model//userplaylist.txt",
                        "C://Users//SOC//Desktop//project//model//userplaylists",
                        user.getName());
                PlaylistUI playlistUI = new PlaylistUI(playlist, user);
                playlistUI.setVisible(true);
            }
        });
    }
}
