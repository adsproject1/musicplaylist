package model.user;

import java.io.*;
import java.util.Scanner;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class User {
    private String name;
    private String email;
    private String password;

    User(String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

}

class Users {
    private static final String USER_DATA_DIRECTORY = "userdata/";

    public Users() {
        File directory = new File(USER_DATA_DIRECTORY);
        if (!directory.exists()) {
            directory.mkdirs();
        }
    }

    public void register(User user) {
        String filename = USER_DATA_DIRECTORY + user.getEmail() + ".txt";
        try (PrintWriter writer = new PrintWriter(filename)) {
            writer.println(user.getName());
            writer.println(user.getEmail());
            writer.println(user.getPassword());
            System.out.println("Registration successful. Welcome, " + user.getName() + "!");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public User login(String email, String password) {
        String filename = USER_DATA_DIRECTORY + email + ".txt";
        try (Scanner scanner = new Scanner(new File(filename))) {
            String storedName = scanner.nextLine(); // Read and store the user's name first
            String storedEmail = scanner.nextLine(); // Read the stored email

            String storedPassword = scanner.nextLine(); // Read the stored password

            if (password.equals(storedPassword)) {
                User loggedInUser = new User(storedName, storedEmail, password);
                System.out.println("Login successful. Welcome, " + storedName + "!");
                return loggedInUser;
            } else {
                System.out.println("Login failed. Incorrect password.");
            }
        } catch (FileNotFoundException e) {
            System.out.println("User not found.");
        }
        return null;
    }

}

class UserLoginUI extends JFrame {
    private JTextField emailField;
    private JPasswordField passwordField;

    private Users users;

    public UserLoginUI(Users users) {
        this.users = users;

        setTitle("User Login");
        setSize(400, 200); // Increased width and height
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        placeComponents(panel);
        add(panel);

        setVisible(true);
    }

    private void placeComponents(JPanel panel) {
        panel.setLayout(null);

        JLabel emailLabel = new JLabel("Email:");
        emailLabel.setBounds(10, 20, 80, 25);
        panel.add(emailLabel);

        emailField = new JTextField(20);
        emailField.setBounds(100, 20, 250, 25); // Increased width
        panel.add(emailField);

        JLabel passwordLabel = new JLabel("Password:");
        passwordLabel.setBounds(10, 50, 80, 25);
        panel.add(passwordLabel);

        passwordField = new JPasswordField(20);
        passwordField.setBounds(100, 50, 250, 25); // Increased width
        panel.add(passwordField);

        JButton loginButton = new JButton("Login");
        loginButton.setBounds(10, 80, 80, 25);
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                loginUser();
            }
        });
        panel.add(loginButton);

        JButton registerButton = new JButton("Register");
        registerButton.setBounds(100, 80, 100, 25);
        registerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                openRegistrationForm();
            }
        });
        panel.add(registerButton);
    }

    private void loginUser() {
    String email = emailField.getText();
    String password = new String(passwordField.getPassword());

    User loggedInUser = users.login(email, password);

    if (loggedInUser != null) {
        // Perform actions after successful login
        // For example, you can open the main application window
        System.out.println("Perform actions after successful login");

        // Create PlaylistUI and make it visible
        Playlist playlist = new Playlist(
                            "C://Users//SOC//Desktop//project//model//userplaylist.txt",
                            "C://Users//SOC//Desktop//project//model//userplaylists",
                            loggedInUser.getName());
        PlaylistUI playlistUI = new PlaylistUI(playlist,loggedInUser);
        playlistUI.setVisible(true);

        // Close the login window
        dispose();
    } else {
        JOptionPane.showMessageDialog(this, "Login failed. Incorrect email or password.");
    }
}


    private void openRegistrationForm() {
        // Code to open the registration form goes here
        // You can create a new JFrame for the registration form or use a dialog
        RegistrationForm registrationForm = new RegistrationForm(users);
        registrationForm.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new UserLoginUI(new Users());
            }
        });
    }
}

class RegistrationForm extends JFrame {
    private JTextField nameField;
    private JTextField emailField;
    private JPasswordField passwordField;

    private Users users;

    public RegistrationForm(Users users) {
        this.users = users;

        setTitle("User Registration");
        setSize(400, 200); // Increased width and height
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        JPanel panel = new JPanel();
        placeComponents(panel);
        add(panel);
    }

    private void placeComponents(JPanel panel) {
        panel.setLayout(null);

        JLabel nameLabel = new JLabel("Name:");
        nameLabel.setBounds(10, 20, 80, 25);
        panel.add(nameLabel);

        nameField = new JTextField(20);
        nameField.setBounds(100, 20, 250, 25); // Increased width
        panel.add(nameField);

        JLabel emailLabel = new JLabel("Email:");
        emailLabel.setBounds(10, 50, 80, 25);
        panel.add(emailLabel);

        emailField = new JTextField(20);
        emailField.setBounds(100, 50, 250, 25); // Increased width
        panel.add(emailField);

        JLabel passwordLabel = new JLabel("Password:");
        passwordLabel.setBounds(10, 80, 80, 25);
        panel.add(passwordLabel);

        passwordField = new JPasswordField(20);
        passwordField.setBounds(100, 80, 250, 25); // Increased width
        panel.add(passwordField);

        JButton registerButton = new JButton("Register");
        registerButton.setBounds(10, 120, 100, 25);
        registerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registerUser();
            }
        });
        panel.add(registerButton);
    }

    private void registerUser() {
        String name = nameField.getText();
        String email = emailField.getText();
        String password = new String(passwordField.getPassword());

        User newUser = new User(name, email, password);
        users.register(newUser);

        JOptionPane.showMessageDialog(this, "Registration successful. Welcome, " + name + "!");
        dispose(); // Close the registration form
    }
}