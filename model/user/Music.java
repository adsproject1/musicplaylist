package model.user;

import java.io.File;
import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        SongManager songManager = new SongManager("C://Users//SOC//Desktop//project//model//listofsong");
        File sourceFolder = new File("C://Users//SOC//Desktop//project//model//songs");

        System.out.println("Welcome to music playlist sorter!");
        Scanner scan = new Scanner(System.in);
        Users users = new Users();

        boolean loggedIn = false;
        User loggedInUser = null;

        while (!loggedIn) {
            System.out.println("Enter 1: For registration");
            System.out.println("Enter 2: For login");

            int option = scan.nextInt();
            scan.nextLine();

            if (option == 1) {
                System.out.println("Registration:");
                System.out.println("Enter your name: ");
                String name = scan.nextLine();
                System.out.println("Enter your email: ");
                String email = scan.nextLine();
                System.out.println("Enter your password: ");
                String password = scan.nextLine();

                User user = new User(name, email, password);
                users.register(user);
            } else if (option == 2) {
                System.out.println("Login:");
                System.out.print("Enter your email: ");
                String email = scan.nextLine();
                System.out.print("Enter your password: ");
                String password = scan.nextLine();

                loggedInUser = users.login(email, password);

                if (loggedInUser != null) {
                    // User successfully logged in, create the playlist
                    Playlist userPlaylist = new Playlist(
                            "C://Users//SOC//Desktop//project//model//userplaylist.txt",
                            "C://Users//SOC//Desktop//project//model//userplaylists",
                            loggedInUser.getName());

                    while (loggedInUser != null) {
                        System.out.println("Enter your option:");
                        System.out.println("1. Add Song");
                        System.out.println("2. Make Song to Playlist");
                        System.out.println("3. Play");
                        System.out.println("4. Next Song");
                        System.out.println("5. Pause Song");
                        System.out.println("6. Search Song");
                        System.out.println("7. Logout");

                        String[] listofsong = songManager.getSongListWithNames();
                        System.out.println("Current List of Songs:");
                        for (String song : listofsong) {
                            System.out.println(song);
                        }

                        int musicOption = scan.nextInt();
                        scan.nextLine();

                        switch (musicOption) {
                            case 1:
                                System.out.println("Enter the song name: ");
                                String songName = scan.nextLine();
                                System.out.println("Enter the artist name: ");
                                String artistName = scan.nextLine();
                                System.out.println("Enter the MP3 file path: ");
                                String mp3FilePath = scan.nextLine();

                                Song userSong = new Song(songName, artistName, mp3FilePath);
                                songManager.addSong(userSong, sourceFolder);

                                listofsong = songManager.getSongListWithNames();

                                System.out.println("Song added successfully!");
                                break;

                            case 2:
                                int count = listofsong.length - 1;
                                System.out.println("Enter the song index to add to the playlist from 0 to : " + count);
                                int songIndex = scan.nextInt();
                                scan.nextLine();

                                if (songIndex >= 0 && songIndex < listofsong.length) {
                                    String selectedSongName = listofsong[songIndex];
                                    // Assuming you have a method to get Song object by name in SongManager
                                    String[] songNames = songManager.getSongListWithNames();
                                    selectedSongName = songNames[songIndex];
                                    Song selectedSong = new Song(selectedSongName, "", ""); // You may want to extract
                                    if (selectedSong != null) {
                                        userPlaylist.addSongToPlaylist(selectedSong,
                                                "C://Users//SOC//Desktop//project//model//userplaylists",
                                                loggedInUser.getName());
                                    }
                                } else {
                                    System.out.println("Invalid song index.");
                                }
                                break;

                            case 3:
                                userPlaylist.playAudio();
                                break;

                            case 4:
                                userPlaylist.playNext();
                                break;

                            case 5:
                                userPlaylist.stopAudio();
                                break;

                            case 6:
                                System.out.println("Enter the keyword to search: ");
                                String keyword = scan.nextLine();
                                userPlaylist.searchAndPlay(keyword);
                                break;

                            case 7:
                                loggedInUser = null;
                                System.out.println("Logged out successfully!");
                                break;

                            default:
                                System.out.println("Invalid option. Please try again.");
                                break;
                        }
                    }
                } else {
                    System.out.println("Invalid login credentials. Please try again.");
                }
            }
        }

        scan.close();
    }
}
