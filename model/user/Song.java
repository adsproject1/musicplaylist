package model.user;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.io.PrintWriter;


class Song {
    private String title;
    private String artist;
    private String filePath;

    public Song(String title, String artist, String filePath) {
        this.title = title;
        this.artist = artist;
        this.filePath = filePath;
    }

    public String getTitle() {
        return title;
    }

    public String getArtist() {
        return artist;
    }

    public String getFilePath() {
        return filePath;
    }
}

class AddSongResult {
    private int index;
    private String songName;

    public AddSongResult(int index, String songName) {
        this.index = index;
        this.songName = songName;
    }

    public int getIndex() {
        return index;
    }

    public String getSongName() {
        return songName;
    }
}

class SongManager {
    private static String storageFolder;
    private Song[] songlists;
    private int playlistSize;

    public  SongManager(String storageFolder) {
        this.storageFolder = storageFolder;
        this.songlists = new Song[10]; // Adjust the initial size based on your needs
        this.playlistSize = 0;

        File folder = new File(storageFolder);
        if (!folder.exists()) {
            folder.mkdirs();
        }
    }

    public AddSongResult addSong(Song song, File sourceFolder) {
    if (isSongInPlaylist(song)) {
        System.out.println("Song already exists in the songlists: " + song.getTitle() + " - " + song.getArtist());
        return new AddSongResult(-1, null); // Return -1 to indicate that the song was not added
    }

    if (playlistSize == songlists.length) {
        // Resize the array if it's full
        songlists = resizeArray();
    }

    String fileName = song.getTitle() + " - " + song.getArtist() + ".mp3";
    File destinationFile = new File(storageFolder, fileName);

    // Construct the source file path correctly
    String sourceFilePath = song.getFilePath(); // assuming it's already a valid absolute path
    File sourceFile = new File(sourceFilePath);

    try {
        Files.copy(sourceFile.toPath(), destinationFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        songlists[playlistSize] = song;
        int addedIndex = playlistSize; // Store the index where the song was added
        playlistSize++;
        System.out.println("Song added successfully: " + fileName);

        // Call getSongListWithNames to update the list after adding the song
        updateListOfSongs();

        return new AddSongResult(addedIndex, fileName);
    } catch (IOException e) {
        System.err.println("Error: Unable to add the song. " + e.getMessage());
        return new AddSongResult(-1, null); // Return -1 to indicate that the song was not added
    }
}

// Add this new method in the SongManager class
private void updateListOfSongs() {
    // Call getSongListWithNames
    String[] songs = getSongListWithNames();

    // Write the list to listofsong directory
    try (PrintWriter writer = new PrintWriter("C://Users//SOC//Desktop//project//model//listofsong")) {
        for (String song : songs) {
            writer.println(song);
        }
    } catch (FileNotFoundException e) {
        System.err.println("Error: Unable to update listofsong. " + e.getMessage());
    }
}


    public static String[] getSongListWithNames() {
    File sourceFolder = new File("C://Users//SOC//Desktop//project//model//listofsong");

    if (!sourceFolder.isDirectory()) {
        System.out.println("Invalid source folder path. Please check the configuration.");
        return new String[0]; // Return an empty array in case of an error
    }

    File[] files = sourceFolder.listFiles();
    StringBuilder songList = new StringBuilder();

    if (files != null) {
        for (File file : files) {
            if (file.isFile() && file.getName().endsWith(".mp3")) {
                songList.append(file.getName()).append("\n");
            }
        }
    }

    // Split the song names directly into an array
    return songList.toString().split("\n");
}



    private boolean isSongInPlaylist(Song song) {
        for (int i = 0; i < playlistSize; i++) {
            if (songlists[i].getTitle().equals(song.getTitle()) && songlists[i].getArtist().equals(song.getArtist())) {
                return true;
            }
        }
        return false;
    }

    private Song[] resizeArray() {
        int newSize = songlists.length * 2; // Adjust the resizing strategy based on your needs
        Song[] newArray = new Song[newSize];
        System.arraycopy(songlists, 0, newArray, 0, songlists.length);
        return newArray;
    }
}
